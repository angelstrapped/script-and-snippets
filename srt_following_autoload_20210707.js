// ==UserScript==
// @name     SRT Following Autoload
// @version  1.0.1
// @match    http://speedruntools.com/following/
// @match    https://speedruntools.com/following/
// @match    https://www.twitch.tv/directory/following/live
// @match    https://www.twitch.tv/directory/game/Death%20Stranding
// ==/UserScript==

setTimeout(function(){
   window.location.reload(1)
}, 300000)

window.addEventListener('load', injectStuff)

function randomHexNumber() {
	return (((Math.random() * 14) + 1)|0).toString(16)
}

function injectStuff() {
	timeSinceLoad = 0;
	color = "#" + randomHexNumber() + randomHexNumber() + randomHexNumber() + randomHexNumber() + randomHexNumber() + randomHexNumber()
	console.log(color)
	incrementTimeSinceLoad(timeSinceLoad, color)
  //sortStreamCardsByTimeSinceLive()
}

/* function incrementTimeSinceLoad(timeSinceLoad, color) {
	document.getElementById('streams-section').firstElementChild.innerHTML = "LAST UPDATED <em style=\"color:" + color + "\">" + timeSinceLoad + "</em> MINUTES AGO";
	
	timeSinceLoad++;
	
	setTimeout(incrementTimeSinceLoad, 60000, timeSinceLoad, color);
} */

async function incrementTimeSinceLoad(timeSinceLoad, color) {
  // Sleep
  await new Promise(r => setTimeout(r, 3000))
  
  var timer_text = "LAST UPDATED <em style=\"color:" + color + "\">" + timeSinceLoad + "</em> MINUTES AGO"
	
  var following_page_element = document.getElementsByClassName('sc-AxirZ ScTitleText-sc-1gsen4-0 ipNmNI tw-title')
  if (following_page_element.length > 0) {
    following_page_element[0].innerHTML = timer_text
  }
  
  var game_page_element = document.getElementsByClassName('sc-AxirZ ScTitleText-sc-1gsen4-0 gAYdrP tw-title')
	if (game_page_element.length > 0) {
    game_page_element[0].innerHTML = game_page_element[0].innerHTML + " " + timer_text
  }

	timeSinceLoad++
	console.log("moop")
 
	setTimeout(incrementTimeSinceLoad, 60000, timeSinceLoad, color)
}

function returnParent(e) {
  return e.parentElement
}

function sortStreamCardsByTimeSinceLive() {
  console.log('doot')
	var liveChannelsCards = document.getElementsByClassName('live-channel-card').map(returnParent)
  console.log(liveChannelsCards)
}
