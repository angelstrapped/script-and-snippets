setTimeout(function(){
   window.location.reload(1);
}, 600000);

window.addEventListener('load', injectStuff);

function randomHexNumber() {
	return (((Math.random() * 14) + 1)|0).toString(16)
}

function injectStuff() {
	timeSinceLoad = 0;
	color = "#" + randomHexNumber() + randomHexNumber() + randomHexNumber() + randomHexNumber() + randomHexNumber() + randomHexNumber();
	console.log(color);
	incrementTimeSinceLoad(timeSinceLoad, color);
}

function incrementTimeSinceLoad(timeSinceLoad, color) {
	document.getElementById('streams-section').firstElementChild.innerHTML = "LAST UPDATED <em style=\"color:" + color + "\">" + timeSinceLoad + "</em> MINUTES AGO";
	
	timeSinceLoad++;
	
	setTimeout(incrementTimeSinceLoad, 60000, timeSinceLoad, color);
}