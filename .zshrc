# Command prompt
# Lots of crazy stuff from Oh My Zsh here, but mostly I want to save the syntax for the colors.
export PS1='%{$terminfo[bold]$fg[grey]%}%n@%{$reset_color%}%m%{$terminfo[bold]$fg[grey]%}:%{$reset_color$fg[magenta]%}%1d%{$reset_color%}$(ruby_prompt_info)$(git_prompt_info)$(virtualenv_prompt_info)%B%{$terminfo[bold]$fg[grey]%}$%{$reset_color%}%b